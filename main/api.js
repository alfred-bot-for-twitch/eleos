var request = require('request');
const jwt = require('jsonwebtoken');

const jwtSecret = process.env.TWITCH_SECRET;

const getUser = (userToken, callback) => {
  var options = {
    url: 'https://api.twitch.tv/helix/users',
    method: 'GET',
    headers: {
      'Client-ID': process.env.TWITCH_CLIENT_ID,
      Accept: 'application/vnd.twitchtv.v5+json',
      Authorization: 'Bearer ' + userToken,
    },
  };

  request(options, callback);
};

const getHasuraAccessToken = (userId, channelId) => {
  console.log(userId);
  console.log(channelId);
  let role = 'viewer';
  if (channelId === userId)
    role = 'broadcaster'
  const claims = {
    sub: userId,
    'https://hasura.io/jwt/claims': {
      'x-hasura-default-role': role,
      'x-hasura-user-id': userId,
      'x-hasura-allowed-roles': ['viewer', 'broadcaster'],
    },
  };
  const token = jwt.sign(claims, jwtSecret, { algorithm: 'HS256'});
  return token;
};

module.exports = {
  getUser: getUser,
  getHasuraAccessToken: getHasuraAccessToken
}
