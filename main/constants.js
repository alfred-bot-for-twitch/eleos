const STRINGS = {
    invalidAuthHeader: 'Invalid authorization header',
    invalidJwt: 'Invalid JWT',
    invalidUserId: 'Invalid User ID',
    twitch: "https://api.twitch.tv/helix"
  };

  exports.modules = STRINGS;