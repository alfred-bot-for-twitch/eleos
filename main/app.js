/*
Copyright 2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License"). You may not use this file except in compliance with the License. A copy of the License is located at

    http://aws.amazon.com/apache2.0/

or in the "license" file accompanying this file. This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

*/

// Define our dependencies
var express = require('express');
var session = require('express-session');
var passport = require('passport');
var OAuth2Strategy = require('passport-oauth').OAuth2Strategy;
var path = require('path');
var hbs = require('hbs');
var { getUser, getHasuraAccessToken } = require('./api');

// Define our constants, you will change these with your own
const SESSION_SECRET = 'jenny';
const CALLBACK_URL_PROD =
  'https://0xggusz0y8.execute-api.eu-central-1.amazonaws.com/latest/auth/twitch/callback';

// You can run locally with - http://localhost:3000/auth/twitch/callback
// const CALLBACK_URL_DEV = 'http://localhost:3000/auth/twitch/callback';

hbs.registerHelper('json', function(obj) {
  return obj.replace(/(['"])/g, '\\$1');
});

// Initialize Express and middlewares
var app = express();
app.use(
  session({ secret: SESSION_SECRET, resave: false, saveUninitialized: false })
);
app.use(passport.initialize());
app.use(passport.session());
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// Override passport profile function to get user profile from Twitch API
OAuth2Strategy.prototype.userProfile = function(accessToken, done) {
  const processUser = function(error, response, body) {
    if (response && response.statusCode == 200) {
      done(null, JSON.parse(body));
    } else {
      done(JSON.parse(body));
    }
  };

  getUser(accessToken, processUser);
};

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

passport.use(
  'twitch',
  new OAuth2Strategy(
    {
      authorizationURL: 'https://id.twitch.tv/oauth2/authorize',
      tokenURL: 'https://id.twitch.tv/oauth2/token',
      clientID: process.env.TWITCH_CLIENT_ID,
      clientSecret: process.env.TWITCH_APP_SECRET,
      callbackURL: CALLBACK_URL_PROD,
      state: true,
    },
    function(accessToken, refreshToken, profile, done) {
      profile.accessToken = accessToken;
      profile.refreshToken = refreshToken;

      // Securely store user profile in your DB
      //User.findOrCreate(..., function(err, user) {
      //  done(err, user);
      //});

      done(null, profile);
    }
  )
);

// Set route to start OAuth link, this is where you define scopes to request
app.get(
  '/auth/twitch',
  passport.authenticate('twitch', { scope: 'user_read' })
);

// Set route for OAuth redirect
app.get(
  '/auth/twitch/callback',
  passport.authenticate('twitch', {
    successRedirect: '/latest/alfy',
    failureRedirect: '/latest/alfy',
  })
);

// If user has an authenticated session, display it, otherwise display link to authenticate
app.get('/alfy', function(req, res) {
  if (req.session && req.session.passport && req.session.passport.user) {
    let user = req.session.passport.user.data[0];
    const token = getHasuraAccessToken(user.id, user.id);
    user = Object.assign({}, user, {
      accessToken: req.session.passport.user.accessToken,
      refreshToken: req.session.passport.user.refreshToken,
      hasuraAccessToken: token,
    });
    res.render('auth', user);
  } else {
    res.send(401, 'Login with Twitch failed');
  }
});

app.get('/tokenize', function(req, res){
  if(req.query.userId){
    const token = getHasuraAccessToken(req.query.userId, req.query.channelId);
    res.header('Access-Control-Allow-Origin', '*').send({
      newToken: token
    });
  }
});

app.listen(3000, function() {
  console.log('Alfred Authentication for Twitch is up now...');
});
module.exports = app;
