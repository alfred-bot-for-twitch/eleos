(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['auth.hbs'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<!DOCTYPE html>\n\n<head>\n    <title>Twitch Authentication Success</title>\n</head>\n<script>\n    \n    function saveToken(){\n        localStorage.setItem('accessToken', '"
    + alias4(((helper = (helper = helpers.accessToken || (depth0 != null ? depth0.accessToken : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"accessToken","hash":{},"data":data}) : helper)))
    + "');\n        localStorage.setItem('refreshToken', \""
    + alias4(((helper = (helper = helpers.refreshToken || (depth0 != null ? depth0.refreshToken : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"refreshToken","hash":{},"data":data}) : helper)))
    + "\");\n        localStorage.setItem('display_name', \""
    + alias4(((helper = (helper = helpers.display_name || (depth0 != null ? depth0.display_name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"display_name","hash":{},"data":data}) : helper)))
    + "\");\n        localStorage.setItem('bio', \""
    + alias4(((helper = (helper = helpers.bio || (depth0 != null ? depth0.bio : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"bio","hash":{},"data":data}) : helper)))
    + "\");\n\n    }\n</script>\n\n<body onload=\"saveToken()\"><button>Go Home</button></body>\n\n</html>";
},"useData":true});
})();